# Dockerfile
FROM python:alpine3.7

ENV PYTHONUNBUFFERED 1

RUN mkdir -p /code
COPY . /code

WORKDIR /code

# Python Application Deps
RUN pip3 install -r requirements.txt

EXPOSE 8000
# command to run
CMD python manage.py runserver 0.0.0.0:8000
