"""training_tool URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from tastypie.api import Api
from application.api.resources import UserResource
from application.api.lessoncalendar import LessonCalendarResource
from application.api.lesson import LessonResource
from application.api.category import CategoryResource
from application.api.subject import SubjectResource
from application.api.course import CourseResource
from application.api.userlesson import UserLessonResource
from application.api.user import UserResource

v1_api = Api(api_name='v1')
v1_api.register(UserResource())
v1_api.register(SubjectResource())
v1_api.register(CategoryResource())
v1_api.register(CourseResource())
v1_api.register(LessonCalendarResource())
v1_api.register(LessonResource())
v1_api.register(UserLessonResource())
v1_api.register(UserResource())


urlpatterns = [
    #refer other URLconfs by include function
    path('', include('application.urls')),
    url(r'^api/', include(v1_api.urls)),
    path('admin/', admin.site.urls)
]
