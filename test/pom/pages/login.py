from test.components.model import Model
from test.components.base import UiObject, UiSearchType


class LoginPage(Model):

    def __init__(self, driver):
        super(LoginPage, self).__init__(driver)
        self.username = UiObject(selector='username', search_type=UiSearchType.id)
        self.password = UiObject(selector='password', search_type=UiSearchType.id)
        self.submit_button = UiObject(selector='submit', search_type=UiSearchType.id)

    def set_username(self, username):
        self.username.set_text(username)

    def set_password(self, password):
        self.password.set_text(password)

    def submit(self):
        self.submit_button.click()