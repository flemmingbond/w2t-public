from test.pom.pages.login import LoginPage


class LoginWorkFlow(LoginPage):
    
    def __init__(self, driver):
        super(LoginWorkFlow, self).__init__(driver)

    def login(self, username, password):
        self.set_username(username)
        self.set_password(password)
        self.submit()
