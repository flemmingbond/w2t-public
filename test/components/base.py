from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
import selenium.common.exceptions as webdriver_ex
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from time import sleep
from test.utils.utils import enum

UiSearchType = enum(css=By.CSS_SELECTOR, xpath=By.XPATH, id=By.ID, name=By.NAME)


class UiObject(object):

    def __init__(self, selector=None, parent=None, search_type=UiSearchType.css, inherit=True, driver=None):
        self.driver = driver
        self.selector = selector
        self.search_type = search_type
        self.parent = parent
        self.parent_index = None
        self.element = None

    def get(self, index=0, retry=20, think_time=0.5):
        try:
            for i in range(retry):
                elements = self.driver.find_elements(by=self.search_type, value=self.selector)
                if elements:
                    self.element = elements
                    break
                else:
                    sleep(think_time)
            if not elements:
                raise webdriver_ex.NoSuchElementException("Retried get elemenets got failed")
        except webdriver_ex.NoSuchElementException as e:
            raise webdriver_ex.NoSuchElementException("Failed to find elements")
        except Exception as e:
            raise webdriver_ex.ErrorInResponseException("Failed to find elements in response")

    def click(self, index=0, timeout=10, method='webdriver'):
        if not self.element:
            self.element = self.get(index)
        self.element.click()

    def set_text(self,text=None, index=0, timeout=10, method='webdriver'):
        if not self.element:
            self.element = self.get(index)
        self.element.send_keys(text)

