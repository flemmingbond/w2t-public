import pytest
from selenium.webdriver import Chrome
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

@pytest.fixture()
def init_driver():
    capabilities = DesiredCapabilities.CHROME
    capabilities['loggingPrefs'] = {'browser': 'ALL'}
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option("excludeSwitches",
                                           ["igonore-certificate-errors"])
    chrome_options.add_argument("--igonore-certificate-errors")
    chrome_driver = '/home/workspace/w2t/test/driver/chromedriver'
    # chrome_driver = '/home/nddung1412/selenium/chromedriver'
    wd = webdriver.Chrome(chrome_driver, desired_capabilities=capabilities)
    wd.implicitly_wait(1)
    wd.set_window_size(1280, 1024)
    return wd
