from pytest_bdd import scenario, given, when, then, parsers, scenarios
from selenium.webdriver import Chrome
from selenium import webdriver
from functools import partial
import sys
import os
# insert at 1, 0 is the script path (or '' in REPL)
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = ROOT_DIR.replace("""\\""", "/")
ROOT_DIR = '/home/workspace/w2t/'
sys.path.insert(1, ROOT_DIR)

from test.pom.pages.login import LoginPage


CONVERTERS = {
    'username': str,
    'password': str
}

scenarios('../../../features/login.feature', example_converters=CONVERTERS)

EXTRA_TYPES = {
    'Number': int,
    'Text': str
}
parse_params = partial(parsers.cfparse, extra_types=EXTRA_TYPES)


@given("User is in login page")
def login_page(init_driver):
    init_driver.get("http://127.0.0.1:8000")
    login = LoginPage(init_driver)
    return login


@when(parse_params('Username "{username:Text}" and password "{password:Text}" were inputted'))
@when('Username "<username>" and password "<password>" were inputted')
def input_username_and_password(login_page, username, password):
    login_page.set_username(username)
    login_page.set_password(password)


@when('User clicks on submit button')
def click_on_submit_button(login_page):
    login_page.submit()


@then('Dashboard page is presented')
def login_successfully(init_driver):
    init_driver.close()
