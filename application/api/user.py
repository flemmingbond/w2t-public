from tastypie.resources import ModelResource, ALL_WITH_RELATIONS
from application.models.user import User
from application.models.subject import Subject
from application.models.user import User
from application.models.lesson_calendar import LessonCalendar
from application.models.course_category import CourseCategory
from application.models.course import Course
from tastypie.authorization import Authorization
from tastypie import fields
from tastypie.authentication import MultiAuthentication, SessionAuthentication
from application.libs.terraapiauthentication import TerraApiAuthentication
from tastypie.bundle import Bundle
from tastypie import http
from tastypie.exceptions import (
    NotFound)
from tastypie.utils import (
    dict_strip_unicode_keys, is_valid_jsonp_callback_value, string_to_python,
    trailing_slash,
)
from django.core.exceptions import (
    ObjectDoesNotExist
)


class UserResource(ModelResource):

    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        authorization = Authorization()
        # authentication = MultiAuthentication(TerraApiAuthentication(), SessionAuthentication())
        list_allowed_methods = ['put', 'get']
        # Check permission for POST/PUT/DELETE
        # authorization = Authorization()
        filtering = {
            "subject_id": ALL_WITH_RELATIONS,
            }
