from tastypie.resources import ModelResource
from application.models.user import User
from application.models.subject import Subject
from application.models.lesson import Lesson
from application.models.lesson_calendar import LessonCalendar
from application.models.course_category import CourseCategory
from application.models.course import Course
from tastypie.authorization import Authorization
from tastypie import fields
from tastypie.authentication import MultiAuthentication, SessionAuthentication
from application.libs.terraapiauthentication import TerraApiAuthentication
from tastypie.bundle import Bundle
from tastypie import http
from tastypie.exceptions import (
    NotFound)
from tastypie.utils import (
    dict_strip_unicode_keys, is_valid_jsonp_callback_value, string_to_python,
    trailing_slash,
)
from django.core.exceptions import (
    ObjectDoesNotExist
)


##   https://django-tastypie.readthedocs.io/en/latest/authentication.html
#https://sam.hooke.me/note/2019/07/migrating-from-tastypie-to-django-rest-framework/
# https://monicalent.com/blog/2014/10/31/django-tastypie-reverse-relationships-filtering/
# https://django-tastypie.readthedocs.io/en/latest/resources.html
#https://books.agiliq.com/projects/django-orm-cookbook/en/latest/existing_database.html


class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        # Authen user
        excludes = ['password']
        authentication = MultiAuthentication(TerraApiAuthentication(), SessionAuthentication())
        list_allowed_methods = ['get']
        # Check permission for POST/PUT/DELETE
        authorization = Authorization()



