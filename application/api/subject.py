from tastypie.resources import ModelResource, ALL_WITH_RELATIONS
from application.models.user import User
from application.models.subject import Subject
from application.models.lesson import Lesson
from application.models.lesson_calendar import LessonCalendar
from application.models.course_category import CourseCategory
from application.models.course import Course
from tastypie.authorization import Authorization
from tastypie import fields
from tastypie.authentication import MultiAuthentication, SessionAuthentication
from application.libs.terraapiauthentication import TerraApiAuthentication
from tastypie.bundle import Bundle
from tastypie import http
from tastypie.exceptions import (
    NotFound)
from tastypie.utils import (
    dict_strip_unicode_keys, is_valid_jsonp_callback_value, string_to_python,
    trailing_slash,
)
from django.core.exceptions import (
    ObjectDoesNotExist
)
# from application.api.category import CategoryResource


class SubjectResource(ModelResource):
    # course_type = fields.ToManyField('application.api.category.CategoryResource', 'course_type', related_name='id')
    course_type_id = fields.ToOneField('application.api.category.CategoryResource', 'course_type', full=True)

    class Meta:
        queryset = Subject.objects.all().order_by("-id")
        resource_name = 'subject'
        authorization = Authorization()
        # authentication = MultiAuthentication(TerraApiAuthentication(), SessionAuthentication())
        list_allowed_methods = ['get', 'post', 'delete', 'put']
        # Check permission for POST/PUT/DELETE
        authorization = Authorization()
        filtering = {
            "course_type_id": ALL_WITH_RELATIONS,
            }

    def delete_detail(self, request, **kwargs):
        """
        Destroys a single resource/object.

        Calls ``obj_delete``.

        If the resource is deleted, return ``HttpNoContent`` (204 No Content).
        If the resource did not exist, return ``Http404`` (404 Not Found).
        """
        # Manually construct the bundle here, since we don't want to try to
        # delete an empty instance.
        bundle = Bundle(request=request)

        try:
            self.obj_delete(bundle=bundle, **self.remove_api_resource_names(kwargs))
            return http.HttpNoContent()
        except NotFound:
            return http.HttpNotFound()

    def obj_delete(self, bundle, **kwargs):
        """
        A ORM-specific implementation of ``obj_delete``.

        Takes optional ``kwargs``, which are used to narrow the query to find
        the instance.
        """
        if not hasattr(bundle.obj, 'delete'):
            try:
                bundle.obj = self.obj_get(bundle=bundle, **kwargs)
            except ObjectDoesNotExist:
                raise NotFound("A model instance matching the provided arguments could not be found.")

        self.authorized_delete_detail(self.get_object_list(bundle.request), bundle)
        _id = bundle.obj.id
        Subject.objects.filter(id=_id).delete()