from django.shortcuts import render, get_object_or_404
from django.db.models import Count
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.http import HttpResponse
from django.views.generic import ListView
from application.models.user import User
from django.contrib.auth.decorators import login_required
from application.models.course_category import CourseCategory
from application.models.subject import Subject
from application.models.course import Course
from application.models.lesson import Lesson
from application.models.user import User
from application.models.lesson_calendar import LessonCalendar
from application.models.user_lesson import UserLesson
from datetime import datetime
from datetime import datetime as dt
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
import pytz
from django.shortcuts import render

from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.urls import reverse

from application.models.document import Document
from application.form import DocumentForm


title = 'Web Traning Tool - Course'
#https://www.django-rest-framework.org/#quickstart


@login_required(login_url='')
def index(request):

    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('application:index'))
    else:
        user = request.session.get('user')
        if user['user_roll'] == "1":
            return HttpResponseRedirect(reverse('application:index'))
        if user['user_roll'] == "2":
            course = Course.objects.all().order_by("-id").filter(teacher_id=user['id']).values()
            user_list = User.objects.all().order_by("-id").filter(id=user['id'])
        else:
            course = Course.objects.all().order_by("-id").values()
            user_list = User.objects.all().order_by("-id")
        subject = Subject.objects.all().order_by("-id").values()
        category_list = CourseCategory.objects.all().values()


        data = {
            'title': title,
            'user_data': user,
            'temp_template': 'course/index.html',
            'root_page': 'Course',
            'child_page': 'Subject',
            'course_list': course,
            'category_list': category_list,
            'user_list': user_list
        }
        return render(request, 'layout/layout_home.html', data)


@login_required(login_url='')
def course_student(request, course_id):

    #https://www.youtube.com/watch?v=zhKPDnDQ7-Q
    #UserLesson.objects.filter(lesson_calendar_id__in=[31,32]).values('user_id').annotate(dcount=Count('user_id'))
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('application:index'))
    else:
        lesson_calendar_dict = LessonCalendar.objects.filter(course_id=course_id).values('id')
        lesson_calendar_list = []
        for i in lesson_calendar_dict:
            lesson_calendar_list.append(i['id'])
        # course = Course.objects.all().order_by("-id").values()
        # subject = Subject.objects.all().order_by("-id").values()
        category_list = CourseCategory.objects.all().values()
        user_joined_dict = UserLesson.objects.filter(lesson_calendar_id__in=lesson_calendar_list).values('user_id').\
            annotate(dcount=Count('user_id'))
        user_joined_dict_list = []
        for i in user_joined_dict:
            user_joined_dict_list.append(i['user_id'])
        user_list = User.objects.all().order_by("-id").values()

        for i in range(len(user_list)):
            if user_list[i]['id'] in user_joined_dict_list:
                user_list[i]['join_status'] = True
            else:
                user_list[i]['join_status'] = False
        user = request.session.get('user')
        course = get_object_or_404(Course, pk=course_id)
        data = {
            'title': title,
            'user_data': user,
            'temp_template': 'course/course_student.html',
            'root_page': 'Course',
            'child_page': 'Subject',
            'course': course,
            'category_list': category_list,
            'user_list': user_list,
            'user_joined_dict_list': user_joined_dict_list
        }
        return render(request, 'layout/layout_home.html', data)


@login_required(login_url='')
def lesson_calendar(request, course_id):

    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('application:index'))
    else:
        lesson_calendar = LessonCalendar.objects.filter(course_id=course_id).values()
        for i in range(len(lesson_calendar)):
            if lesson_calendar[i]['time_start']:
                lesson_calendar[i]['time_start'] = int(lesson_calendar[i]['time_start'])/1000
                d = datetime.fromtimestamp(lesson_calendar[i]['time_start'])
                d = pytz.timezone('Asia/Bangkok').localize(d)
                # d = d.astimezone(pytz.itc)
                lesson_calendar[i]['time_start'] = d.strftime('%m/%d/%Y')
                # ts = int("1284101485")
                # lesson_calendar[i]['time_start'] = datetime.utcfromtimestamp(ts).strftime('%m/%d/%Y')

        course = get_object_or_404(Course, pk=course_id)
        subject = Subject.objects.all().order_by("-id").values()
        category_list = CourseCategory.objects.all().values()
        user_list = User.objects.all().order_by("-id").values()
        user = request.session.get('user')
        data = {
            'title': title,
            'user_data': user,
            'temp_template': 'course/lesson_calendar.html',
            'root_page': 'Course',
            'child_page': 'Subject',
            'course': course,
            'category_list': category_list,
            'user_list': user_list,
            'lesson_calendar': lesson_calendar
        }
        return render(request, 'layout/layout_home.html', data)


def lesson_calendar(request, course_id):

    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('application:index'))
    else:
        lesson_calendar = LessonCalendar.objects.filter(course_id=course_id).values()
        for i in range(len(lesson_calendar)):
            if lesson_calendar[i]['time_start']:
                lesson_calendar[i]['time_start'] = int(lesson_calendar[i]['time_start'])/1000
                d = datetime.fromtimestamp(lesson_calendar[i]['time_start'])
                d = pytz.timezone('Asia/Bangkok').localize(d)
                # d = d.astimezone(pytz.itc)
                lesson_calendar[i]['time_start'] = d.strftime('%m/%d/%Y')
                # ts = int("1284101485")
                # lesson_calendar[i]['time_start'] = datetime.utcfromtimestamp(ts).strftime('%m/%d/%Y')

        course = get_object_or_404(Course, pk=course_id)
        subject = Subject.objects.all().order_by("-id").values()
        category_list = CourseCategory.objects.all().values()
        user_list = User.objects.all().order_by("-id").values()
        user = request.session.get('user')
        data = {
            'title': title,
            'user_data': user,
            'temp_template': 'course/lesson_calendar.html',
            'root_page': 'Course',
            'child_page': 'Subject',
            'course': course,
            'category_list': category_list,
            'user_list': user_list,
            'lesson_calendar': lesson_calendar
        }
        return render(request, 'layout/layout_home.html', data)


def my_course(request):

    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('application:index'))
    else:
        user = request.session.get('user')
        course = Course.objects.all().order_by("-id").values()
        course_joined = []
        for i in course:
            lesson_calendar_dict = LessonCalendar.objects.filter(course_id=i['id']).values('id')
            lesson_calendar_list = []
            for j in lesson_calendar_dict:
                lesson_calendar_list.append(j['id'])
            u_lesson = UserLesson.objects.filter(user_id=user['id'], lesson_calendar_id__in=lesson_calendar_list)
            if len(u_lesson) > 0:
                course_joined.append(i)
        data = {
            'title': title,
            'user_data': user,
            'temp_template': 'course/my_course.html',
            'root_page': 'Home',
            'child_page': 'Dashboard',
            'course_list': course_joined,
            'other': i['id'],
        }
        return render(request, 'layout/layout_home.html', data)


@api_view(['PUT', 'GET', 'DELETE'])
def course_add_student(request):
    """

    :param request:
    user_id: Student join course
    course_id: Course to join
    :param pk:
    :return:
    """
    user_id = request.data['user_id']
    course_id = request.data['course_id']
    lesson_calendar_dict = LessonCalendar.objects.filter(course_id=course_id).values('id')
    lesson_calendar_list = []
    for i in lesson_calendar_dict:
        lesson_calendar_list.append(i['id'])
    user_joined_dict = UserLesson.objects.filter(lesson_calendar_id__in=lesson_calendar_list).values('user_id'). \
        annotate(dcount=Count('user_id'))
    user_joined_dict_list = []
    for i in user_joined_dict:
        user_joined_dict_list.append(i['user_id'])
    if int(user_id) in user_joined_dict_list and request.method == 'PUT':
        return Response('User already create lesson calendar', status=status.HTTP_400_BAD_REQUEST)

    if int(user_id) not in user_joined_dict_list and request.method == 'DELETE':
        return Response('User not in lesson calendar', status=status.HTTP_404_NOT_FOUND)

    for i in lesson_calendar_dict:
        data = {}
        data['lesson_calendar_id'] = i['id']
        data['user_id'] = user_id
        if request.method == 'PUT':
            user_lesson = UserLesson(**data)
            user_lesson.save()
        elif request.method == 'DELETE':
            UserLesson.objects.filter(lesson_calendar_id__in=lesson_calendar_list).filter(user_id=user_id).delete()

        # lesson_calendar_list.append(i['id'])

    return Response(status=status.HTTP_204_NO_CONTENT)

@login_required(login_url='')
def course_details(request, course_id):

    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('application:index'))
    else:
        d = datetime.today()
        curent = d.strftime('%m/%d/%Y')
        user = request.session.get('user')
        course = get_object_or_404(Course, pk=course_id)
        lesson_calendar_dict = LessonCalendar.objects.filter(course_id=course_id).values('id', 'lesson_name')
        lesson_calendar_list = []
        for i in lesson_calendar_dict:
            lesson_calendar_list.append(i['id'])
        user_lesson_calendar_dict = UserLesson.objects.select_related('lesson_calendar').\
            filter(lesson_calendar_id__in=lesson_calendar_list).filter(user_id=user['id']).\
            values("id", "lesson_calendar_id", "user_id", "lesson_calendar__lesson_name", \
                   "lesson_calendar__time_start", "time_checkin")
        for i in range(len(user_lesson_calendar_dict)):
            user_lesson_calendar_dict[i]['status'] = ""
            # Check if lesson calendar was configured issue date
            if user_lesson_calendar_dict[i]['lesson_calendar__time_start']:

                user_lesson_calendar_dict[i]['lesson_calendar__time_start'] = \
                    int(user_lesson_calendar_dict[i]['lesson_calendar__time_start'])/1000
                d = datetime.fromtimestamp(user_lesson_calendar_dict[i]['lesson_calendar__time_start'])
                d = pytz.timezone('Asia/Bangkok').localize(d)
                # d = d.astimezone(pytz.itc)
                # Convert start time to format %m/%d/%Y
                user_lesson_calendar_dict[i]['lesson_calendar__time_start'] = d.strftime('%m/%d/%Y')
                current_date = dt.strptime(curent, "%m/%d/%Y")
                lesson_date = dt.strptime(user_lesson_calendar_dict[i]['lesson_calendar__time_start'], "%m/%d/%Y")

                # Having 3 case:
                # Current time less than start time, then mark not start and ignore checking
                if current_date < lesson_date:
                    user_lesson_calendar_dict[i]['status'] = 'Not start'

                # Current time equals to start time, so open checking action
                elif current_date == lesson_date:
                    # If Student checked, show status
                    if user_lesson_calendar_dict[i]['time_checkin'] != 'null'\
                            and user_lesson_calendar_dict[i]['time_checkin']:
                        user_lesson_calendar_dict[i]['status'] = 'Checked'
                    else:
                        user_lesson_calendar_dict[i]['status'] = '\
                            <button class="btn btn-primary btn-sm" onclick="checkingLesson(%s,%s)">\
                            Checking\
                            </button>' % (user_lesson_calendar_dict[i]['id'], user['id'])
                # Current time greater than start time, so mark class is closed.
                else:
                    if user_lesson_calendar_dict[i]['time_checkin'] == 'null' or not \
                            user_lesson_calendar_dict[i]['time_checkin']:
                        user_lesson_calendar_dict[i]['status'] = 'Closed'
                    else:
                        user_lesson_calendar_dict[i]['status'] = 'Checked'

        user_joined_dict = UserLesson.objects.select_related('user').filter(
            lesson_calendar_id__in=lesson_calendar_list). \
            values('user_id', "user__user_name").annotate(dcount=Count('user_id'))



        documents = Document.objects.filter(course_id=course_id)
        user = request.session.get('user')
        data = {
            'title': title,
            'user_data': user,
            'temp_template': 'course/details.html',
            'root_page': 'Course',
            'child_page': 'Subject',
            'course': course,
            'user_lesson_calendar_dict': user_lesson_calendar_dict,
            'documents': documents,
            'user_joined_dict': user_joined_dict,
            'lesson_calendar_dict': lesson_calendar_dict
        }
        return render(request, 'layout/layout_home.html', data)


def list(request):
    # Handle file upload
    course = Course.objects.get(id=13)
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(docfile = request.FILES['docfile'], course_id=course, doc_name="Test")
            newdoc.save()

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('list'))
    else:
        form = DocumentForm() # A empty, unbound form

    # Load documents for the list page
    documents = Document.objects.filter(course_id=13)


    # Render list page with the documents and the form
    return render(request, 'list.html', {'documents': documents, 'form': form})


@api_view(['POST', 'DELETE', 'GET'])
def document(request):

    if request.method == 'POST':
        course_id = request.data['course_id']
        course = Course.objects.get(id=course_id)
        doc_name = request.data['doc_name']
        newdoc = Document(docfile=request.FILES['file'], course_id=course, doc_name=doc_name)
        newdoc.save()
    elif request.method == 'DELETE':
        course_id = request.data['course_id']
        document_id = request.data['document_id']
        Document.objects.filter(id=document_id).filter(course_id=course_id).delete()
    elif request.method == 'GET':
        documents = Document.objects.all().values()
        return Response(documents)
    return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def filter_user_lesson(request, user_id, lesson_calendar_id, course_id):

    d = datetime.today()
    curent = d.strftime('%m/%d/%Y')
    course = get_object_or_404(Course, pk=course_id)
    if lesson_calendar_id:
        lesson_calendar_list = LessonCalendar.objects.filter(course_id=course_id).filter(
            id=lesson_calendar_id).values_list('id')
    else:
        lesson_calendar_list = LessonCalendar.objects.filter(course_id=course_id).values_list('id')

    if user_id:
        user_lesson_calendar_dict = UserLesson.objects.select_related('lesson_calendar').select_related('user').\
            filter(lesson_calendar_id__in=lesson_calendar_list).filter(user_id=user_id). \
            values("id", "lesson_calendar_id", "user_id", "lesson_calendar__lesson_name","lesson_calendar__time_start",
                   "time_checkin", "user__user_name")
    else:
        user_lesson_calendar_dict = UserLesson.objects.select_related('lesson_calendar').select_related('user').\
            filter(lesson_calendar_id__in=lesson_calendar_list).values("id", "lesson_calendar_id", "user_id",
            "lesson_calendar__lesson_name","lesson_calendar__time_start", "time_checkin", "user__user_name")

    for i in user_lesson_calendar_dict:
        i['status'] = ""
        # Check if lesson calendar was configured issue date
        if i['lesson_calendar__time_start']:

            i['lesson_calendar__time_start'] = \
                int(i['lesson_calendar__time_start']) / 1000
            d = datetime.fromtimestamp(i['lesson_calendar__time_start'])
            d = pytz.timezone('Asia/Bangkok').localize(d)
            # d = d.astimezone(pytz.itc)
            # Convert start time to format %m/%d/%Y
            i['lesson_calendar__time_start'] = d.strftime('%m/%d/%Y')
            current_date = dt.strptime(curent, "%m/%d/%Y")
            lesson_date = dt.strptime(i['lesson_calendar__time_start'], "%m/%d/%Y")

            # Having 3 case:
            # Current time greater than start time, then mark not start and ignore checking
            if current_date < lesson_date:
                i['status'] = 'Not start'

            # Current time equals to start time, so open checking action
            else:
                # If Student checked, show status
                if i['time_checkin'] != 'null' and i['time_checkin']:
                    i['status'] = 'Checked'
                else:
                   i['status'] = '\
                        <button class="btn btn-primary btn-sm" onclick="teacherCheckingLesson(%s,%s)">\
                        Checking\
                        </button>' % (i['id'], user_id)

    return Response(user_lesson_calendar_dict)
