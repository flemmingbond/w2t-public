from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.http import HttpResponse
from django.views.generic import ListView
from application.models.user import User
from django.contrib.auth.decorators import login_required
from application.models.course import Course
from application.models.user_lesson import UserLesson
from application.models.lesson_calendar import LessonCalendar

title = 'Web Traning Tool - Dashboard'


@login_required(login_url='')
def index(request):

    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('application:index'))
    else:
        user = request.session.get('user')
        course = Course.objects.all().order_by("-id").values()
        course_not_joined = []
        for i in course:
            lesson_calendar_dict = LessonCalendar.objects.filter(course_id=i['id']).values('id')
            lesson_calendar_list = []
            for j in lesson_calendar_dict:
                lesson_calendar_list.append(j['id'])
            u_lesson = UserLesson.objects.filter(user_id=user['id'], lesson_calendar_id__in=lesson_calendar_list)
            if len(u_lesson) == 0:
                course_not_joined.append(i)
        data = {
            'title': title,
            'user_data': user,
            'temp_template': 'home/dashboard.html',
            'root_page': 'Home',
            'child_page': 'Dashboard',
            'course_list': course_not_joined,
            'other': i['id'],
        }
        return render(request, 'layout/layout_home.html', data)