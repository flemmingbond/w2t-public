from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.http import HttpResponse
from django.views.generic import ListView
from application.models.user import User
from django.contrib.auth.decorators import login_required
from application.models.course_category import CourseCategory
from application.models.subject import Subject
from application.models.lesson import Lesson
title = 'Web Traning Tool - Subject'


@login_required(login_url='')
def index(request):

    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('application:index'))
    else:
        subject = Subject.objects.all().order_by("-id").values()
        category_list = CourseCategory.objects.all().values()
        user = request.session.get('user')
        data = {
            'title': title,
            'user_data': user,
            'temp_template': 'subject/index.html',
            'root_page': 'Course',
            'child_page': 'Subject',
            'subject_list': subject,
            'category_list': category_list
        }
        return render(request, 'layout/layout_home.html', data)


@login_required(login_url='')
def lesson(request, subject_id):

    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('application:index'))
    else:
        subject = get_object_or_404(Subject, pk=subject_id)
        lesson = Lesson.objects.filter(subject_id=subject_id).values()
        user = request.session.get('user')
        data = {
            'title': title,
            'user_data': user,
            'temp_template': 'lesson/index.html',
            'root_page': 'Course',
            'child_page': 'Subject',
            'subject_object': subject,
            'lesson_list': lesson
        }
        return render(request, 'layout/layout_home.html', data)