from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.http import HttpResponse
from django.views.generic import ListView
from application.models.user import User
from django.contrib.auth.decorators import login_required
from application.models.course_category import CourseCategory
from application.models.user import User
title = 'Web Traning Tool - User'


@login_required(login_url='')
def index(request):

    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('application:index'))
    else:
        user_list = User.objects.all().order_by("-id").values()
        category_list = CourseCategory.objects.all().values()
        user = request.session.get('user')
        user_roles = [{'id': "1", 'name': 'Student'},
                      {'id': "2", 'name': 'Teacher'},
                      {'id': "3", 'name': 'Admin'}
                      ]
        data = {
            'title': title,
            'user_data': user,
            'temp_template': 'user/index.html',
            'root_page': 'Course',
            'child_page': 'Subject',
            'user_list': user_list,
            'user_roles': user_roles
        }
        return render(request, 'layout/layout_home.html', data)

