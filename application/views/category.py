from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.http import HttpResponse
from django.views.generic import ListView
from application.models.user import User
from django.contrib.auth.decorators import login_required
from application.models.course_category import CourseCategory
title = 'Web Traning Tool - Subject'


@login_required(login_url='')
def index(request):

    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('application:index'))
    else:
        category_list= CourseCategory.objects.all().order_by("-id").values()
        user_list = User.objects.all().order_by("-id").values()
        user = request.session.get('user')
        data = {
            'title': title,
            'user_data': user,
            'temp_template': 'category/index.html',
            'root_page': 'Course',
            'child_page': 'Subject',
            'category_list': category_list,
            'user_list': user_list
        }
        return render(request, 'layout/layout_home.html', data)