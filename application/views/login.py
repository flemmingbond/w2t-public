from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from application.models.user import User
from django.urls import reverse
from django.shortcuts import redirect
from application.libs.common_libs import terra_authentication
from django.contrib.auth.models import User as AuthenUser
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout as django_logout


##Document

##   https://django-tastypie.readthedocs.io/en/latest/authentication.html
#https://sam.hooke.me/note/2019/07/migrating-from-tastypie-to-django-rest-framework/
# https://monicalent.com/blog/2014/10/31/django-tastypie-reverse-relationships-filtering/
# https://django-tastypie.readthedocs.io/en/latest/resources.html
#https://books.agiliq.com/projects/django-orm-cookbook/en/latest/existing_database.html

def index(request):
    user = request.session.get('user')
    if not user:
        return render(request, 'layout/layout_login.html')
    else:
        return HttpResponseRedirect(reverse('application:home'))


def verify_login(request):
    try:
        username = request.POST['username']
        password = request.POST['password']
        request_api = terra_authentication(username, password)
        if request_api.status_code == 200:
            user_data = (request_api.json())['additionalInformation']
            user = User.objects.filter(email=username).values()
            if not user:
                #  create user if not existed
                u = User(user_name=username, password='', email=username, user_roll='1')
                u.save()
                auth_user = AuthenUser.objects.create_user(username=username,
                                                           email=username,
                                                           password=password)
                user = User.objects.filter(email=username).values()
            merge_user_data = {**user_data, **user[0]}
        else:
            return render(request, 'layout/layout_login.html', {
                'error_message': "Wrong username or password.",
            })
        user = authenticate(username=username, password=password)
        if not user:
            return render(request, 'layout/layout_login.html', {
                'error_message': "Wrong username or password.",
            })
        login(request, user)
        request.session['user'] = merge_user_data
    except KeyError as e:
        return render(request, 'layout/layout_login.html', {
            'error_message': "Please enter username and password.",
        })
    else:
        return HttpResponseRedirect(reverse('application:home'))


def verify_login_without_auth(request):
    try:
        username = request.POST['username']
        password = request.POST['password']
        user = User.objects.filter(email=username).values()
        user_data = {}
        if not user:
            #  create user if not existed
            u = User(user_name=username, password='', email=username, user_roll='1')
            u.save()
            auth_user = AuthenUser.objects.create_user(username=username,
                                                       email=username,
                                                       password=password)
            user = User.objects.filter(email=username).values()

        merge_user_data = {**user_data, **user[0]}
        user = authenticate(username=username, password=password)
        if not user:
            return render(request, 'layout/layout_login.html', {
                'error_message': "Wrong username or password.",
            })
        login(request, user)
        request.session['user'] = merge_user_data
    except KeyError as e:
        return render(request, 'layout/layout_login.html', {
            'error_message': "Please enter username and password.",
        })
    else:
        return HttpResponseRedirect(reverse('application:home'))


def logout(request):
    if request.session.get('user', True):
        django_logout(request)
        request.session.flush()
    return redirect('application:index')

