function PostAjax(url, form_data){
    return $.ajax({
              type : 'POST',
              url: url,
//              dataType: 'text',
//              cache: false,
//              contentType: 'application/x-www-form-urlencoded',
//              processData: false,
            data: JSON.stringify(form_data),
            contentType: "application/json",
           });
}

function PutAjax(url, form_data){
    return $.ajax({
              type : 'PUT',
              url: url,
//              dataType: 'text',
//              cache: false,
//              contentType: 'application/x-www-form-urlencoded',
//              processData: false,
            data: JSON.stringify(form_data),
            contentType: "application/json",
           });
}

function DeleteAjax(url, form_data){
    return $.ajax({
              type : 'DELETE',
              url: url,
//              dataType: 'text',
//              cache: false,
//              contentType: 'application/x-www-form-urlencoded',
//              processData: false,
            data: JSON.stringify(form_data),
            contentType: "application/json",
           });
}

function GetAjax(url){
      return $.ajax({
      type : 'GET',
      url  : url,
      contentType: 'application/json',
      });
}

var _id;
var _deleteFunc
var myEditor;
var type;
function ConfirmDialog(title, message, id) {
    $('#confirmModal').modal('toggle');
    $('#confirmModalTitle').html(title)
    $('#confirmModalBody').html(message)
    _id = id
};

$('.confirm-close').click(function(){
    $('#confirmModal').modal('toggle');
});

$('#confirmModalConfirm').click(function(){
    _deleteFunc(_id)
    $('#confirmModal').modal('toggle');
});

function ResultDialog(title, message) {
    $('#resultModal').modal('toggle');
    $('#resultModalTitle').html(title)
    $('#resultModalBody').html(message)
};

// Javscripts for Category page
//1 -----------------------------------------------------------------------------------Category-------------
function LoadCategoryTable()
{
    $('tbody').empty();
    var table_html = "";
    var url = "/api/v1/category/";
    $.when(GetAjax(url)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status == 200){
//            parser_objects = JSON.parse(data)
            SubjectObjects = data.objects
            $.each(SubjectObjects, function(key,val){
                table_html += '<tr class="tr-shadow">'
                table_html += '<td>'+ val.id +'</td>'
                table_html += '<td>'+ val.course_type +'</td>'
                table_html += '<td> \
                                        <label class="switch switch-3d switch-success mr-3"> \
                                          <input type="checkbox" class="switch-input" checked="true"> \
                                          <span class="switch-label"></span> \
                                          <span class="switch-handle"></span> \
                                        </label> \
                                    </td>'
                table_html += '<td> \
                                    <div class="table-data-feature"> \
                                        <button class="item" onclick="callUpdateCategory('+ val.id +')" data-placement="top" title="Edit" data-toggle="modal" data-target="#UpdateCategoryModal"> \
                                                <i class="zmdi zmdi-edit"></i> \
                                        </button> \
                                        <button class="item" onclick="callDeleteCategory('+ val.id +')" data-toggle="tooltip" data-placement="top" title="Delete"> \
                                            <i class="zmdi zmdi-delete"></i> \
                                        </button> \
                                    </div> \
                                </td>'
                 table_html += '</tr>'
                table_html += '<tr class="spacer"></tr>'
            });
            $( "tbody" ).append(table_html);
        }
        else{
            return;
        }
    });
}

function callDeleteCategory(categoryId)
{
    _deleteFunc = deleteCategory
    ConfirmDialog('Delete category ' + categoryId + '?', 'Please confirm that you want to delete this object',
    categoryId);


}
function deleteCategory(categoryId)
{
    var form_data = {}
    var url = "/api/v1/category/" + categoryId + "/";
    form_data.id = categoryId;
    $.when(DeleteAjax(url, form_data)).then(function( data, textStatus, jqXHR ) {
            if(jqXHR.status == 204){
                LoadCategoryTable();
            }
            else{
               ResultDialog('Error', 'Failed to Delete object ' + data)
            }
    });
}
$('#submit-create-category').click(function(event) {
    var form_data = {}
    form_data.course_type = $('#course_type').val();
    $(".email-loading").fadeIn();
    var url = "/api/v1/category/";
    $.when(PostAjax(url, form_data)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status == 201){
            $(".email-loading").fadeOut();
            LoadCategoryTable();
            $('#course_type').val('')
            $('#createCategoryModal').modal('toggle');
        }
        else{
            $(".email-loading").fadeOut();
            $('#createCategoryModal').modal('toggle');
        }
    });
});
var edit_id
function callUpdateCategory(id){
    $(".email-loading").fadeIn();
    var url = "/api/v1/category/" + id + '/';
    $.when(GetAjax(url)).then(function( data, textStatus, jqXHR ) {
        $('#edit_course_type').val(data.course_type)
        $(".email-loading").fadeOut();
        edit_id = id
    });
}
$('#submit-update-category').click(function(event) {
    var form_data = {}
    form_data.course_type = $('#edit_course_type').val();
    $(".email-loading").fadeIn();
    var url = "/api/v1/category/" + edit_id + '/';
    $.when(PutAjax(url, form_data)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status == 204){
            $(".email-loading").fadeOut();
            LoadCategoryTable();
            $('#edit_course_type').val('')
            $('#UpdateCategoryModal').modal('toggle');
        }
        else{
            $(".email-loading").fadeOut();
            $('#UpdateCategoryModal').modal('toggle');
        }
    });
});

//2. ---------------------Subject-----------------------------------------
$('#submit-create-subject').click(function(event) {
    var form_data = {}
    $(".email-loading").fadeIn();
    form_data.subject_name = $('#subject_name').val();
    course_type_id = $('#course_type_id').val();
    var url = "/api/v1/category/" + course_type_id + '/'
    $.when(GetAjax(url)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status != 200){
            $(".email-loading").fadeOut();
            return;
        }else{
            form_data.course_type_id = data
            var url = "/api/v1/subject/";
                $.when(PostAjax(url, form_data)).then(function( data, textStatus, jqXHR ) {
                    if(jqXHR.status == 201){
                        $(".email-loading").fadeOut();
                        LoadSubjectTable();
                        $('#subject_name').val('')
                        $('#createSubjectModal').modal('toggle');
                    }
                    else{
                        $(".email-loading").fadeOut();
                        $('#createSubjectModal').modal('toggle');
                    }
                });
        }
    });
});


function LoadSubjectTable()
{
    $('tbody').empty();
    var table_html = "";
    var url = "/api/v1/subject/";
    $.when(GetAjax(url)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status == 200){
//            parser_objects = JSON.parse(data)
            SubjectObjects = data.objects
            var no = 1;
            $.each(SubjectObjects, function(key,val){
                table_html += '<tr class="tr-shadow">'
                table_html += '<td>'+ no +'</td>'
                table_html += '<td>'+ val.subject_name +'</td>'
                table_html += '<td>'+ val.course_type_id.course_type +'</td>'
                table_html += '<td> \
                                    <a href="/lesson/'+ val.id +'" class="btn btn-primary btn-sm">Edit</a> \
                                </td>'
                table_html += '<td> \
                                        <label class="switch switch-3d switch-success mr-3"> \
                                          <input type="checkbox" class="switch-input" checked="true"> \
                                          <span class="switch-label"></span> \
                                          <span class="switch-handle"></span> \
                                        </label> \
                                    </td>'
                table_html += '<td> \
                                    <div class="table-data-feature"> \
                                        <button class="item" onclick="callUpdateSubject('+ val.id +')" data-toggle="modal" data-target="#UpdateSubjectModal" data-placement="top" title="Edit"> \
                                                <i class="zmdi zmdi-edit"></i> \
                                            </button> \
                                        <button class="item" onclick="callDeleteSubject('+ val.id +')" data-toggle="tooltip" data-placement="top" title="Delete"> \
                                                <i class="zmdi zmdi-delete"></i> \
                                            </button> \
                                    </div> \
                                </td>'
                 table_html += '</tr>'
                table_html += '<tr class="spacer"></tr>'
                no += 1
            });
            $( "tbody" ).append(table_html);
        }
        else{
            return;
        }
    });
}
var edit_id
function callUpdateSubject(id){
    $(".email-loading").fadeIn();
    var url = "/api/v1/subject/" + id + '/';
    $.when(GetAjax(url)).then(function( data, textStatus, jqXHR ) {
        $('#edit_subject_name').val(data.subject_name)
        $('#edit_course_type_id').val(data.course_type_id.id)
        $(".email-loading").fadeOut();
        edit_id = id
    });
}
$('#submit-update-subject').click(function(event) {
    var form_data = {}
    form_data.subject_name = $('#edit_subject_name').val();
    course_type_id = $('#edit_course_type_id').val();
    var url = "/api/v1/category/" + course_type_id + '/'
    $.when(GetAjax(url)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status != 200){
            $(".email-loading").fadeOut();
            return;
        }else{
            form_data.course_type_id = data
            var url = "/api/v1/subject/" + edit_id + '/';
                $.when(PutAjax(url, form_data)).then(function( data, textStatus, jqXHR ) {
                    if(jqXHR.status == 204){
                        $(".email-loading").fadeOut();
                        LoadSubjectTable();
                        $('#UpdateSubjectModal').modal('toggle');
                    }
                    else{
                        $(".email-loading").fadeOut();
                        $('#UpdateSubjectModal').modal('toggle');
                    }
                });
        }
    });
});
var subjectId;
function callDeleteSubject(subjectId)
{
    _deleteFunc = deleteSubject
    ConfirmDialog('Delete subject ' + subjectId + '?', 'Please confirm that you want to delete this object',
    subjectId);
}
function deleteSubject(subjectId)
{
    var form_data = {}
    var url = "/api/v1/subject/" + subjectId + "/";
    form_data.id = subjectId;
    $.when(DeleteAjax(url, form_data)).then(function( data, textStatus, jqXHR ) {
            if(jqXHR.status == 204){
                LoadSubjectTable();
            }
            else{
               ResultDialog('Error', 'Failed to Delete object ' + data)
            }
    });
}

//3------------------------------------------------Lesson-------------------------------------------------------


var _subject_id
function callCreateLesson(subject_id){
    _subject_id = subject_id
    var form_data = {}
    $(".email-loading").fadeIn();
    form_data.lesson_name = $('#lesson_name').val();
    form_data.subject_id = "/api/v1/subject/" + _subject_id + '/';
    var url = "/api/v1/lesson/";
        $.when(PostAjax(url, form_data)).then(function( data, textStatus, jqXHR ) {
            if(jqXHR.status == 201){
                $(".email-loading").fadeOut();
                LoadLessonTable();
                $('#lesson_name').val('')
                $('#createLessonModal').modal('toggle');
            }
            else{
                $(".email-loading").fadeOut();
                $('#createLessonModal').modal('toggle');
            }
        });
}

function LoadLessonTable()
{
    subject_id = $('#subject_id').val()
    $('tbody').empty();
    var table_html = "";
    var url = "/api/v1/lesson/?subject_id=" + subject_id;
    $.when(GetAjax(url)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status == 200){
//            parser_objects = JSON.parse(data)
            SubjectObjects = data.objects
            $.each(SubjectObjects, function(key,val){
                table_html += '<tr class="tr-shadow">'
                table_html += '<td>'+ val.id +'</td>'
                table_html += '<td>'+ val.lesson_name +'</td>'
                table_html += '<td> \
                                    <a href="" class="btn spinner-up btn-sm btn-warning"> \
                                        <i class="fa fa-angle-up"></i> \
                                    </a> \
                                    <a href="" class="btn spinner-down btn-sm btn-danger"> \
                                        <i class="fa fa-angle-down"></i> \
                                    </a> \
                                </td>'
                table_html += '<td> \
                                    <div class="table-data-feature"> \
                                        <button class="item" onclick="callUpdateLesson('+ val.id +')" data-toggle="modal" data-target="#UpdateLessonModal" data-placement="top" title="Edit"> \
                                            <i class="zmdi zmdi-edit"></i> \
                                        </button> \
                                        <button class="item" onclick="callDeleteLesson('+ val.id + ')" data-toggle="tooltip" data-placement="top" title="Delete"> \
                                            <i class="zmdi zmdi-delete"></i> \
                                        </button> \
                                    </div> \
                                </td>'
                 table_html += '</tr>'
                table_html += '<tr class="spacer"></tr>'
            });
            $( "tbody" ).append(table_html);
        }
        else{
            return;
        }
    });
}
var edit_id
function callUpdateLesson(id){
    $(".email-loading").fadeIn();
    var url = "/api/v1/lesson/" + id + '/';
    $.when(GetAjax(url)).then(function( data, textStatus, jqXHR ) {
        $('#edit_lesson_name').val(data.lesson_name)
        $(".email-loading").fadeOut();
        edit_id = id
    });
}
$('#submit-update-lesson').click(function(event) {
    var form_data = {}
    form_data.lesson_name = $('#edit_lesson_name').val();
    form_data.id = edit_id
    var url = "/api/v1/lesson/" + edit_id + '/';
        $.when(PutAjax(url, form_data)).then(function( data, textStatus, jqXHR ) {
            if(jqXHR.status == 204){
                $(".email-loading").fadeOut();
                LoadLessonTable();
                $('#UpdateLessonModal').modal('toggle');
            }
            else{
                $(".email-loading").fadeOut();
                $('#UpdateLessonModal').modal('toggle');
            }
        });

});
var subjectId;
function callDeleteLesson(lessonId)
{
    _deleteFunc = deleteLesson
    ConfirmDialog('Delete subject ' + lessonId + '?', 'Please confirm that you want to delete this object',
    lessonId);
}
function deleteLesson(lessonId)
{
    var form_data = {}
    var url = "/api/v1/lesson/" + lessonId + "/";
    form_data.id = lessonId;
    $.when(DeleteAjax(url, form_data)).then(function( data, textStatus, jqXHR ) {
            if(jqXHR.status == 204){
                LoadLessonTable();
            }
            else{
               ResultDialog('Error', 'Failed to Delete object ' + data)
            }
    });
}


//---------------------------------------Course management -----------------------------
var courseId;
$("#create-course").submit(function(e){
    e.preventDefault();
    var form_data = {}
    form_data.course_name = $('#course_name').val();
    form_data.course_type_id = $('#course_type_id').val();
    form_data.subject_id = $('#subject_id').val();
    form_data.end_time = $('#datepicker-to').val();
    form_data.limited = $('#limited').val();
    form_data.start_time = $('#datepicker-from').val();
    form_data.teacher_id = $('#teacher_id').val();
    form_data.description = myEditor.getData();;
    form_data.created_by = 1;


    $(".email-loading").fadeIn();

    course_type_id = $('#course_type_id').val();
    var url = "/api/v1/category/" + course_type_id + '/'
    $.when(GetAjax(url)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status != 200){
            $(".email-loading").fadeOut();
            return;
        }else{
                form_data.course_type_id = data
                if(type == 'edit'){
                    var url = "/api/v1/course/" + courseId + "/";
                    $.when(PutAjax(url, form_data)).then(function( data, textStatus, jqXHR ) {
                        if(jqXHR.status == 204){
                            $(".email-loading").fadeOut();
                            LoadCourseTable();
                            $('#subject_name').val('')
                            $('#createCourseModal').modal('toggle');
                        }
                        else{
                            $(".email-loading").fadeOut();
                            $('#createCourseModal').modal('toggle');
                        }
                    });
                }
                else if(type == 'create')
                {
                    var url = "/api/v1/course/";
                    $.when(PostAjax(url, form_data)).then(function( data, textStatus, jqXHR ) {
                        if(jqXHR.status == 201){
                            $(".email-loading").fadeOut();
                            LoadCourseTable();
                            $('#subject_name').val('')
                            $('#createCourseModal').modal('toggle');
                        }
                        else{
                            $(".email-loading").fadeOut();
                            $('#createCourseModal').modal('toggle');
                        }
                    });
                }

        }
    });
});
function updateSubject(e)
{
    console.log(e)
    var url = "/api/v1/subject/?course_type_id=" + e;
    $.when(GetAjax(url)).then(function( data, textStatus, jqXHR ) {
    SubjectObjects = data.objects
    $('#subject_id').empty();
    $.each(SubjectObjects, function(key,val){
          $('#subject_id')
            .append('<option value="'+ val.id +'">'+ val.subject_name +'</option>');
        })
    });

}

$('#btnCreateCourse').click(function(){
    type = 'create'
})

function LoadCourseTable()
{
    $('tbody').empty();
    var table_html = "";
    var url = "/api/v1/course/";
    $.when(GetAjax(url)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status == 200){
//            parser_objects = JSON.parse(data)
            SubjectObjects = data.objects
            $.each(SubjectObjects, function(key,val){
                table_html += '<tr class="tr-shadow">'
                table_html += '<td>'+ val.course_name +'</td>'
                table_html += '<td><span class="block-email">lori@example.com</span></td>'
                table_html += '<td>'+ val.start_time +'</td>'
                table_html += '<td>'+ val.end_time +'</td>'
                table_html += '<td> \
                                    <a href="lesson/'+ val.id +'" class="btn btn-primary btn-sm">Add</a> \
                                </td>'
                table_html += '<td> \
                                    <a href="lesson_calendar/'+ val.id +'" class="btn btn-primary btn-sm">Add</a> \
                                </td>'
                table_html += '<td> \
                                    <div class="table-data-feature">\
                                        <button class="item" callUpdateSubjecttitle="Edit" onclick="callUpdateCourse('+ val.id +')" data-toggle="modal" data-target="#createCourseModal">\
                                            <i class="zmdi zmdi-edit"></i>\
                                        </button>\
                                        <button class="item" data-toggle="tooltip" onclick="callDeleteCourse('+ val.id +')" data-placement="top" title="Delete">\
                                            <i class="zmdi zmdi-delete"></i>\
                                        </button>\
                                    </div>\
                                </td>'
                 table_html += '</tr>'
                table_html += '<tr class="spacer"></tr>'
            });
            $( "tbody" ).append(table_html);
        }
        else{
            return;
        }
    });
}

function callDeleteCourse(courseId)
{
    _deleteFunc = deleteCourse
    ConfirmDialog('Delete category ' + courseId + '?', 'Please confirm that you want to delete this object',
    courseId);
}

function deleteCourse(courseId)
{
    var form_data = {}
    var url = "/api/v1/course/" + courseId + "/";
    form_data.id = courseId;
    $.when(DeleteAjax(url, form_data)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status == 204){
            LoadCourseTable();
        }
        else{
           ResultDialog('Error', 'Failed to Delete object ' + data)
        }
    });
}
function callUpdateCourse(id){
    type = 'edit'
    $(".email-loading").fadeIn();
    var url = "/api/v1/course/" + id + '/';
    $.when(GetAjax(url)).then(function( data, textStatus, jqXHR ) {
        $('#course_name').val(data.course_name)
        $('#course_type_id').val(data.course_type_id.id)
        $('#subject_id').val(data.subject_id)
        $('#datepicker-from').val(convertYearMonthToMonthYear(data.start_time))
        $('#datepicker-to').val(convertYearMonthToMonthYear(data.end_time))
        $('#limited').val(data.limited)
        $('#teacher_id').val(data.teacher_id)
        myEditor.data.set(data.description);
//        $('#descriptions').val(data.description)
        $(".email-loading").fadeOut();
        courseId = id
    });
}

//-----------------------------------Lesson calendar------------------
$(".datepicker").blur(function(){
    old_value = $(this).val()
    element = $(this)
    elemeent_id = element.attr('id')
    elemeent_id = elemeent_id.split("-").pop();
    update_id = $('#updateLessonCa-' + elemeent_id)
    console.log(elemeent_id)
    setTimeout(function(){
        if(old_value != element.val())
        {
            console.log(new Date(element.val()).getTime())
    //        element.addClass("bg-warning")
            update_id.addClass("bg-warning")
            update_id.removeAttr("Disabled")
        }
    //    else{
    //        element.removeClass("bg-warning")
    //        update_id.removeClass("bg-warning")
    //    }
        console.log(element.val())
    },500);
});

function callUpdateLessonCa(lessonca_id)
{
    var form_data = {}
    form_data.time_start = new Date($('#start-time-' + lessonca_id).val()).getTime();
    form_data.id = lessonca_id
    update_id = $('#updateLessonCa-' + lessonca_id)
    var url = "/api/v1/lessoncalendar/" + lessonca_id + '/';
        $.when(PutAjax(url, form_data)).then(function( data, textStatus, jqXHR ) {
            if(jqXHR.status == 204){
                update_id.removeClass("bg-warning")
            }
            else{
            }
        });
}
//var myDate="26-02-2012";
//myDate=myDate.split("-");
//var newDate=myDate[1]+"/"+myDate[0]+"/"+myDate[2];
//alert(new Date(newDate).getTime());

//-----------------------------------course student page--------------------
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function removeStudentCalendar(userId, courseId)
{
    var form_data = {}
    form_data.user_id = userId;
    form_data.course_id = courseId;

    var url = '/course_add_student';
    $.when(DeleteAjaxToken(url, form_data)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status == 204){
//            alert('Done')
            $('#leave-course-btn-' + userId).css("display", "none");
            $('#join-course-btn-' + userId).css("display", "block");
        }
        else{
            alert('Failed')
        }
    });
}
function addStudentCalendar(userId, courseId){
    var form_data = {}
    form_data.user_id = userId;
    form_data.course_id = courseId;

    var url = '/course_add_student';
    $.when(PutAjaxToken(url, form_data)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status == 204){
            $('#leave-course-btn-' + userId).css("display", "block");
            $('#join-course-btn-' + userId).css("display", "none");
        }
        else{
            alert('Failed')
        }
    });
}
function GetAjax(url){
      return $.ajax({
      type : 'GET',
      url  : url,
      headers: {'X-CSRFToken': csrftoken},
      contentType: "application/json",
      });
}


function PutAjaxToken(url, form_data){
    return $.ajax({
              type : 'PUT',
              url: url,
//              dataType: 'text',
//              cache: false,
//              contentType: 'application/x-www-form-urlencoded',
//              processData: false,
            data: JSON.stringify(form_data),
            headers: {'X-CSRFToken': csrftoken},
            contentType: "application/json",
           });
}
function DeleteAjaxToken(url, form_data){
    return $.ajax({
              type : 'DELETE',
              url: url,
//              dataType: 'text',
//              cache: false,
//              contentType: 'application/x-www-form-urlencoded',
//              processData: false,
            data: JSON.stringify(form_data),
            headers: {'X-CSRFToken': csrftoken},
            contentType: "application/json",
           });
}

function GetAjaxToken(url){
      return $.ajax({
      type : 'GET',
      url  : url,
      contentType: 'application/json',
      headers: {'X-CSRFToken': csrftoken}
      });
}

//-----------------------Home page------------------
function joinCourse(userId, courseId){
    var form_data = {}
    form_data.user_id = userId;
    form_data.course_id = courseId;

    var url = '/course_add_student';
    $.when(PutAjaxToken(url, form_data)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status == 204){
            location.reload();
        }
        else{
            alert('Failed')
        }
    });
}

function leaveCourse(userId, courseId){
    var form_data = {}
    form_data.user_id = userId;
    form_data.course_id = courseId;

    var url = '/course_add_student';
    $.when(DeleteAjaxToken(url, form_data)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status == 204){
            location.reload();
        }
        else{
            alert('Failed')
        }
    });
}

function checkingLesson(lessonCalendarId, userId){
    var form_data = {}
    form_data.user_id = userId;
    form_data.id = lessonCalendarId;
    form_data.time_checkin = parseInt(Date.now() /1000);

    var url = '/api/v1/userlesson/' + lessonCalendarId + '/';
    $.when(PutAjaxToken(url, form_data)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status == 204){
            location.reload();
        }
        else{
            alert('Failed')
        }
    });
}

$("#formUploadDocument").submit(function(e){
    e.preventDefault();
    $(".email-loading").fadeIn();
    data = new FormData();
    data.append('file', $('#file')[0].files[0]);
    data.append('course_id', $('#courseId').val());
    data.append('doc_name', $('#doc_name').val());
    $.ajax({
      url: "/document",
      type: "POST",
      data: data,
      enctype: 'multipart/form-data',
      headers: {'X-CSRFToken': csrftoken},
      processData: false,  // tell jQuery not to process the data
      contentType: false   // tell jQuery not to set contentType
    }).done(function(data, textStatus, jqXHR) {
        if(jqXHR.status == 204){
            $(".email-loading").fadeOut();
            $('#uploadDocument').modal('toggle');
            LoadDocument()
            return;
        }else{
            $(".email-loading").fadeOut();
            $('#uploadDocument').modal('toggle');

        }
    })
});

function callDeleteDocument(documentId)
{
    _deleteFunc = deleteDocument
    ConfirmDialog('Delete document ' + documentId + '?', 'Please confirm that you want to delete this object',
    documentId);
}

function deleteDocument(documentId)
{
    var form_data = {}
    var url = "/document";
    form_data.document_id = documentId;
    form_data.course_id = $('#courseId').val();
    $.when(DeleteAjaxToken(url, form_data)).then(function( data, textStatus, jqXHR ) {
            if(jqXHR.status == 204){
                LoadDocument()
            }
            else{
               ResultDialog('Error', 'Failed to Delete object ' + data)
            }
    });
}

function LoadDocument()
{
    var href = window.location.href
    var words = href.split('/');
    href = words[0] + words[1] + '//'+ words[2]  + '/media/'
    $('.document').empty();
    var document_html = "";
    var url = "/document";
    $.when(GetAjax(url)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status == 200){
//            parser_objects = JSON.parse(data)
            documentObjects = data
            $.each(documentObjects, function(key,val){
                if(parseInt(val.course_id_id) == parseInt($('#courseId').val()))
                {
                    document_html += '<li class="list-group-item"> \
                                        <a href="'+ href + val.docfile + '" target="_blank">'+ val.doc_name +'\
                                            <span class="badge badge-danger pull-right">Download</span>\
                                        </a>\
                                        <div class="table-data-feature float-right">\
                                            <button class="item" data-toggle="tooltip" onclick="callDeleteDocument('+ val.id + ')" data-placement="top" title="" data-original-title="Delete">\
                                                <i class="zmdi zmdi-delete"></i>\
                                            </button>\
                                        </div>\
                                  </li>'
                }

            });
            $( ".document" ).append(document_html);
        }
        else{
            return;
        }
    });
}
//var href = window.location.href
//var words = href.split('/');
//console.log(words[0] + words[1] + '//'+ words[2]  + '/media/');


$('#filter_user_lesson').click(function(){
    console.log($('#user').val())
    console.log($('#lesson_calendar').val())
    console.log($('#courseId').val())
    var table_html = "";
    var url = "/filter_user_lesson/" + $('#user').val() + '/' + $('#lesson_calendar').val() + '/' + $('#courseId').val()
    $('.user_lesson').empty();
    $.when(GetAjax(url)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status == 200){
            console.log(data)
            $.each(data, function(key,val){
                table_html += '<tr class="tr-shadow"> \
                                <td>'+ val.lesson_calendar__lesson_name +'</td> \
                                <td> \
                                        '+ val.user__user_name +' \
                                </td> \
                                <td> \
                                        '+ val.lesson_calendar__time_start +' \
                                </td> \
                                <td> \
                                    <span class="status--process">Processed</span> \
                                </td> \
                                <td> \
                                    '+ val.status +' \
                                </td> \
                            </tr> \
                            <tr class="spacer"></tr>'
                 table_html += '</tr>'
                table_html += '<tr class="spacer"></tr>'
            });
            $( ".user_lesson" ).append(table_html);
        }
        else{
            return;
        }
    });

});

function teacherCheckingLesson(lessonCalendarId, userId){
    var form_data = {}
    form_data.user_id = userId;
    form_data.id = lessonCalendarId;
    form_data.time_checkin = parseInt(Date.now() /1000);

    var url = '/api/v1/userlesson/' + lessonCalendarId + '/';
    $.when(PutAjaxToken(url, form_data)).then(function( data, textStatus, jqXHR ) {
        if(jqXHR.status == 204){
            $('#filter_user_lesson').click();
        }
        else{
            alert('Failed')
        }
    });
}
ClassicEditor
    .create( document.querySelector( '#descriptions' ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        myEditor = editor;
    } )
    .catch( err => {
        //console.error( err.stack );
    } );

function convertYearMonthToMonthYear(time){
    //Inout YYYY-MM-DD
    //Output MM-DD-YYYY
    time_split = time.split("-");
    return time_split[1] + '/' + time_split[2] + '/' + time_split[0]

}

function callUpdateUserRole(userId)
{
    var form_data = {}
    console.log("Call update")
    form_data.user_roll = $('#selectUserRole-' + userId).val()
    form_data.id = userId
    update_id = $('#updateUserRole-' + userId)
    var url = "/api/v1/user/" + userId + '/';
        $.when(PutAjax(url, form_data)).then(function( data, textStatus, jqXHR ) {
            if(jqXHR.status == 204){
                update_id.removeClass("bg-warning")
            }
            else{
            }
        });
}

$(".user-role").change(function(){

    element = $(this)
    elemeent_id = element.attr('id')
    elemeent_id = elemeent_id.split("-").pop();
    update_id = $('#updateUserRole-' + elemeent_id)
    update_id.addClass("bg-warning")
});