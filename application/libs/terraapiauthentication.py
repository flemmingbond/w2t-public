from tastypie.authentication import Authentication
from tastypie.http import HttpUnauthorized
from django.conf import settings
from django.contrib.auth import authenticate
import base64
import warnings

class TerraApiAuthentication(Authentication):
    """
    Handles HTTP Basic auth against a specific auth backend if provided,
    or against all configured authentication backends using the
    ``authenticate`` method from ``django.contrib.auth``.

    Optional keyword arguments:

    ``backend``
        If specified, use a specific ``django.contrib.auth`` backend instead
        of checking all backends specified in the ``AUTHENTICATION_BACKENDS``
        setting.
    ``realm``
        The realm to use in the ``HttpUnauthorized`` response.  Default:
        ``django-tastypie``.
    """
    auth_type = 'basic'

    def __init__(self, backend=None, realm='django-tastypie', **kwargs):
        super(TerraApiAuthentication, self).__init__(**kwargs)
        self.backend = backend
        self.realm = realm

    def _unauthorized(self):
        response = HttpUnauthorized()
        # FIXME: Sanitize realm.
        response['WWW-Authenticate'] = 'Basic Realm="%s"' % self.realm
        return response

    def extract_credentials(self, request):
        data = self.get_authorization_data(request)
        data = base64.b64decode(data).decode('utf-8')
        username, password = data.split(':', 1)

        return username, password

    def is_authenticated(self, request, **kwargs):
        """
        Checks a user's basic auth credentials against the current
        Django auth backend.

        Should return either ``True`` if allowed, ``False`` if not or an
        ``HttpResponse`` if you need something custom.
        """
        try:
            username, password = self.extract_credentials(request)
        except ValueError:
            return self._unauthorized()

        if not username or not password:
            return self._unauthorized()

        if self.backend:
            user = self.backend.authenticate(
                username=username,
                password=password
            )
        else:
            if not self.require_active and 'django.contrib.auth.backends.ModelBackend' in settings.AUTHENTICATION_BACKENDS:
                warnings.warn("Authenticating inactive users via ModelUserBackend not supported for Django >= 1.10")
            user = authenticate(username=username, password=password)

        if user is None:
            return self._unauthorized()

        # Kept for backwards-compatibility with Django < 1.10
        if not self.check_active(user):
            return False

        request.user = user
        return True
