import requests


def terra_authentication(username, password):
    url = "https://intranet.terralogic.com/oauth/token"
    data = {
        'username': username,
        'password': password,
        'grant_type': 'password',
        'client_id': 'restapp',
        'client_secret': 'restapp',
        'token_device': '2323795',
        'api_key': '2323795'
    }
    headers = {'Content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    request = requests.post(url, data=data, headers=headers)
    return request


if __name__ == '__main__':
    terra_authentication('1', '2')