from django.urls import path

from application.views import views, login, dashboard, subject, category, course, user
from django.conf import settings
from django.conf.urls.static import static
app_name = 'application'

urlpatterns = [
    path('', login.index, name='index'),
    path('login', login.verify_login_without_auth, name='verify_login'),
    path('home', dashboard.index, name='home'),
    path('logout', login.logout, name='logout'),
    path('course', dashboard.index, name='course'),
    path('my_course', course.my_course, name='my_course'),
    path('subject', subject.index, name='subject'),
    path('category', category.index, name='category'),
    path('course_management', course.index, name='course_management'),
    path('user', user.index, name='user'),
    path('report', dashboard.index, name='report'),
    path('lesson/<int:subject_id>', subject.lesson, name='lesson'),
    path('course_student/<int:course_id>', course.course_student, name='course_student'),
    path('lesson_calendar/<int:course_id>', course.lesson_calendar, name='lesson_calendar'),
    path('course_details/<int:course_id>', course.course_details, name='course_details'),
    path('course_add_student', course.course_add_student, name='course_add_student'),
    path('document', course.document, name='document'),
    path('filter_user_lesson/<int:user_id>/<int:lesson_calendar_id>/<int:course_id>', course.filter_user_lesson,
         name='filter_user_lesson'),
    path('list', course.list, name='list')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)