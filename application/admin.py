from django.contrib import admin

# Register your models here.

from application.models.user import User


admin.site.register(User)