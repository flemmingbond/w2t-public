from .base import BaseModel
from django.db import models
from application.models.course_category import CourseCategory


class Subject(BaseModel):
    course_type = models.ForeignKey(CourseCategory, models.DO_NOTHING)
    subject_name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'subject'