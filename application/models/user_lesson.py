from .base import BaseModel
from django.db import models
from application.models.lesson_calendar import LessonCalendar
from application.models.user import User


class UserLesson(BaseModel):
    lesson_calendar = models.ForeignKey(LessonCalendar, models.DO_NOTHING)
    user = models.ForeignKey(User, models.DO_NOTHING)
    time_checkin = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_lesson'