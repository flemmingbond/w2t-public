from .base import BaseModel
from django.db import models


class Lesson(BaseModel):
    subject = models.ForeignKey('Subject', models.DO_NOTHING)
    lesson_name = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'lesson'