from django.db import models


class BaseModel(models.Model):

    class Meta:
        abstract = True

    def __str__(self):
        return str(self.id)

    def create(self):
        self.save()

    def update(self):
        self.save()

    def delete(self):
        self.save()

    def get(self, *args):
        pass
