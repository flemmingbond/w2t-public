from .base import BaseModel
from django.db import models
from application.models.lesson import Lesson
from application.models.course import Course



class LessonCalendar(BaseModel):
    lesson = models.ForeignKey(Lesson, models.DO_NOTHING)
    course = models.ForeignKey(Course, models.DO_NOTHING)
    lesson_name = models.CharField(max_length=100)
    time_start = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'lesson_calendar'
