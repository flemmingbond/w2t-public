from .base import BaseModel
from django.db import models


class CourseCategory(BaseModel):
    course_type = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'course_category'