from .base import BaseModel
from django.db import models
from application.models.lesson_calendar import LessonCalendar
from application.models.user import User
from application.models.course import Course


class Document(BaseModel):

    docfile = models.FileField(upload_to='documents/%Y/%m/%d')
    course_id = models.ForeignKey(Course, models.DO_NOTHING)
    doc_name = models.CharField(max_length=100)