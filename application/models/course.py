from .base import BaseModel
from django.db import models

class Course(BaseModel):
    course_type = models.ForeignKey('CourseCategory', models.DO_NOTHING)
    course_name = models.CharField(max_length=50)
    subject_id = models.IntegerField()
    start_time = models.DateField(blank=True, null=True)
    end_time = models.DateField(blank=True, null=True)
    teacher_id = models.IntegerField()
    limited = models.IntegerField(blank=True, null=True)
    created_by = models.IntegerField()
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'course'